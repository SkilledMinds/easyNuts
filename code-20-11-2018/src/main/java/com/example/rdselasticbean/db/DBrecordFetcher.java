package com.example.rdselasticbean.db;

import java.sql.*;
import java.util.ArrayList;


public class DBrecordFetcher
{

  public static ArrayList<String> fetchrecordsFromRDS()
  {
	  ArrayList<String> al= new ArrayList<String>();
    try
    {
      // create our mysql database connection
      String myDriver = "com.mysql.jdbc.Driver";
      String myUrl = "jdbc:mysql://squbeRnD.cf9f4xndu7fc.ap-south-1.rds.amazonaws.com/squbeRnD";
      Class.forName(myDriver);
      Connection conn = DriverManager.getConnection(myUrl, "squbeRnD_1", "squbeRnD");
      
      // our SQL SELECT query. 
      // if you only need a few columns, specify them by name instead of using "*"
      String query = "SELECT * FROM departments";

      // create the java statement
      Statement st = conn.createStatement();
      
      // execute the query, and get a java resultset
      ResultSet rs = st.executeQuery(query);
         
      // iterate through the java resultset
      while (rs.next())
      {
       
        String firstName = rs.getString("dept_no");
        String lastName = rs.getString("dept_name");
        String record=firstName+" "+lastName;
        System.out.println(" Fetched record :"+ record);
        al.add(record);
      }
      st.close();
    }
    catch (Exception e)
    {
      System.err.println("Got an exception! ");
      e.printStackTrace();
    }
    return al;
  }
}
