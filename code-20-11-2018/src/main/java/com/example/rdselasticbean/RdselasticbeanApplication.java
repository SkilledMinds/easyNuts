package com.example.rdselasticbean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RdselasticbeanApplication {

    public static void main(String[] args) {
        SpringApplication.run(RdselasticbeanApplication.class, args);
    }
}
