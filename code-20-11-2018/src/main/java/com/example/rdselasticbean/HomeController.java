package com.example.rdselasticbean;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.rdselasticbean.db.DBrecordFetcher;

@Controller
public class HomeController {

  
    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        System.out.println(" Displaying welcome jsp");
        model.put("listDepts", DBrecordFetcher.fetchrecordsFromRDS());
        return "welcome";
    }
    
    
    // enable this mapping as your requirement
    
   /* @GetMapping("/hello")
    public String home(Locale locale, Model model) {
        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
        String formattedDate = dateFormat.format(date);
        model.addAttribute("serverTime", formattedDate);
        return "welcome.jsp";
    }


    @GetMapping("/bye")
    public String homeTo(Locale locale, Model model) {
        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
        String formattedDate = dateFormat.format(date);
        model.addAttribute("serverTime", formattedDate);
        return "index.html";
    }
    
    @RequestMapping("/showList")
    public String index() {
        //registry.addViewController("/").setViewName("index.html");
       // registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return "welcome4";
    }*/

}